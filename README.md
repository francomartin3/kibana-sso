# Kibana Single Sign On

Since there is no formal way to embed dashboards without either bypassing all elasticsearch security or having a double log in, here we are. 
The full explanation is at https://discuss.elastic.co/t/proposal-for-a-formal-solution-to-embedding-kibana-without-double-authentication/249215


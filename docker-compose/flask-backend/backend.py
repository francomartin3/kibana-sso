from flask import Flask, request, redirect
from werkzeug.http import parse_authorization_header
import json
import requests
import redis
import uuid
from  werkzeug.datastructures import Headers
from flask_cors import CORS

app = Flask(__name__)
CORS(app, send_wildcard=True)
pool = redis.ConnectionPool(host="redis",
                            port=6379,
                            db=0,
                            encoding=u'utf-8',
                            decode_responses=True)
redis = redis.Redis(connection_pool=pool)

@app.route('/auth')
"""
This would be your corporate SSO. Here we dont validate anything but you would have to send data to ldap to validate.
We focus on doing the double sign in, even though kibana will also be validating against ldap we need the cookies.
"""
def authenticate():
    # Get authorization header
    auth_header = request.headers.get('Authorization')
    # Transform it to user dict
    creds = parse_authorization_header(auth_header)
    print("User {} logged in successfully".format(creds["username"]))
    # Generate token
    token = uuid.uuid1()
    response = {
        'token': str(token)
    }
    # Send credentials to kibana
    header = {
        'Authorization': auth_header
    }
    kibana_response = requests.get("http://kibana:5601", headers=header)
    # Flask saves all three cookies with one key separated by comma. The problem is that the keys have commas.
    # We the first cookie has no expiration date so theres no comma.
    # The second and third cookie have expiration dates that include commas, so we concatenate them.
    security_storage = kibana_response.headers['set-cookie'].split(',')[0]
    security_preferences = "{}{}".format(
        kibana_response.headers['set-cookie'].split(',')[1],
        kibana_response.headers['set-cookie'].split(',')[2]
        )
    security_authentication = "{}{}".format(
            kibana_response.headers['set-cookie'].split(',')[3],
            kibana_response.headers['set-cookie'].split(',')[4]
            )
    # Prepare Hashset for redis
    redis_hset = {
        'username': str(creds["username"]),
        'security_storage': security_storage,
        'security_authentication': security_authentication,
        'security_preferences': security_preferences
    }
    # Write to redis
    redis_write(token, redis_hset)
    # Return response.
    return json.dumps(response)

@app.route("/redirect-iframe")
def redirection():
    # Token comes as query parameter
    token = request.args.get('token')
    # Using token, get cookies and data from redis
    user = redis_read(token)
    print("User: {} got a token successfully".format(user["username"]))
    # Set response headers with cookies from kibana
    headers = Headers()
    headers.add("set-cookie",user['security_storage'])
    headers.add("set-cookie",user['security_authentication'])
    headers.add("set-cookie",user['security_preferences'])
    # Your dashboard
    dashboard = "yourdashboard"
    # Setup redirection
    response = redirect("/app/kibana#/dashboard/{}/".format(dashboard))
    # Flask redirection sucks
    response.data = str("<!DOCTYPE html>"
                        "<html>"
                        "<head>"
                        "<!-- HTML meta refresh URL redirection -->"
                        "<meta http-equiv= \"refresh\""
                        "content=\"0; url=/app/kibana#/dashboard/{}/\">"
                        "</head>"
                        "</html>".format(dashboard))
    # Add headers
    response.headers = headers
    # Finish
    return response


def redis_write(token, hset):
    try:
        redis.hset(str(token), mapping=hset)
        redis.expire(str(token), 600)
    except Exception as err:
        print("Could not save to redis. ", str(err))


def redis_read(token):
    try:
        user = redis.hgetall(token)
    except Exception as err:
        print("Could not read from redis.", str(err))
        user = None
    return user

